const expect = require('expect.js');

const lib = require('../src/index');
 
describe('flat', () => {
  
  it('function flat([1,2,3]) should return [1,2,3]', () => {
    expect( lib.flat([1,2,3]) ).to.eql( [1,2,3]);
  });

  it('function flat( [1,[2,3]] ) should return [1,2,3]', () => {
    expect( lib.flat( [1,[2,3]]) ).to.eql( [1,2,3] );
  });

  it('function flat( [[1,2,[3]],4] ) should return [1,2,3,4]', () => {
    expect( lib.flat( [[1,2,[3]],4] ) ).to.eql( [1,2,3,4] );
  });

  it('function flat( [[1,2,[3,9,[7,8]]],4] ) should return [1,2,3,9,7,8,4]', () => {
    expect( lib.flat( [[1,2,[3,9,[7,8]]],4] ) ).to.eql( [1,2,3,9,7,8,4] );
  });

});
