
exports.flat = (array) => {
  let out = []
  array.forEach(item => {
    if (item instanceof Array){
      out = out.concat( exports.flat(item) );
    }else {
      out.push(item)
    }
  });    
  return out;
}
