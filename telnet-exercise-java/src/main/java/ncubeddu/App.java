package ncubeddu;

import net.nbug.hexprobe.server.telnet.EasyTerminal;
import net.nbug.hexprobe.server.telnet.EasyShellServer;
import net.nbug.hexprobe.server.telnet.EasyShellServer.Command;
import java.io.IOException;
import java.lang.Exception;

/**
 * Telnet Server
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        System.out.println( "Telnet server!" );
        EasyShellServer srv = new EasyShellServer();

        srv.registerCommand("echo", new EasyShellServer.Command() {
            @Override
            public void execute(String name, String argument, EasyTerminal terminal) throws IOException {
                terminal.writeLine("XXXXX" + argument);
                terminal.flush();
            }
        });

        srv.start(10000);
    }

}
