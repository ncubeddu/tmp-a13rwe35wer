const expect = require('expect.js');
var lib = require('../src/telnetServer');
var net = require('net');

var client1 = null;
var client2 = null;

const server = lib.server;
const clients = lib.clients
const PORT = lib.PORT;

let initialized = false;

describe('telnet tests',  () => {

  describe('create server',  () =>  {
    before( (done) => {
      lib.createTelnetServer();

      client1 = net.connect({port: PORT}, () => {
        client2 = net.connect({port: PORT}, () => {
          client1.on('data',  (messageReceived) => {
            client2.on('data',  (messageReceived) => {
              if(!initialized){
                done();
                initialized = true;
              }
            })
          });
        });
      });
    });

    after( (done) => {
      // no way to turn off the server :-(

      // server.close( () => {
      //   done();  
      // })

      done(); 
    });

    it('client2 should receive the message from client1', (done) => {
      var messageSent = 'CIAO';
      client2.on('data',  (messageReceived) => {
        const messageReceivedUTF8 = messageReceived.toString('utf8');
        expect(messageReceivedUTF8.includes(messageSent)).to.be.true;
        console.log(`client2 has received the message ${messageReceivedUTF8} from client1` )
        done();
      });

      client1.write(messageSent);
    });

    it('client1 should receive the message from client2', (done) => {
      var messageSent = 'MIAO';
      client1.on('data',  (messageReceived) => {
        const messageReceivedUTF8 = messageReceived.toString('utf8');
        expect(messageReceivedUTF8.includes(messageSent)).to.be.true;
        console.log(`client1 has received the message ${messageReceivedUTF8} from client2` )
        done();
      });

      client2.write(messageSent);
    });



  });
});