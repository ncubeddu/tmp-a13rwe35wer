var telnet = require('telnet')


// this is the list of connected telnet clients
let clients = [];
let server = null;
const PORT = 10000;

const createTelnetServer = () => {

  console.log(`Starting up Telnet server at port ${PORT}`)

  telnet.createServer( (client) => {
 
    client.do.transmit_binary()
  
    // when receiving data from a client I have to broadcast it to the others
    client.on('data', message =>  {
  
      // if(message === 'exit'){
      //   client.end( () => {
      //     clients = clients.filter(c => c !== client);
      //     console.log (`A guest has disconnected, the number of attendees now is ${clients.length}`)
      //   });
      // }

      clients.forEach( (connectedClient, index) => {
        // I don't want to send the command to myself, only to others
        if(client != connectedClient){
          //console.log (`guest #${index}: ${message}`)
          connectedClient.write(`guest #${index}: ${message}`)
        }
      })
    })
  
    client.write('You are now connected to Telnet server! \n')
      
    // Adding the just connected client to the list of connected clients 
    clients.push(client);
  
    client.write(`The number of attendees is ${clients.length} \n`)
    console.log (`A new guest has connected, the number of attendees now is ${clients.length}`)
   
  }).listen(PORT)
  
}



// Expose for tests
exports.createTelnetServer = createTelnetServer;
exports.clients = clients;
exports.Server = server;
exports.PORT = PORT;

module.exports = exports;
